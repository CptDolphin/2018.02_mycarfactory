package pl.anicos.patterns.exercises.factorymethod.MyCarFactory;

public class Car {
    private long carSeriesNumber = System.currentTimeMillis();

    public String buildingEngine() {
        return "Engine for model " + carSeriesNumber + " has just been build";
    }

    public String buildingChassis() {
        return "Chassis for model " + carSeriesNumber + " has just been build";
    }

    public String buildingBody() {
        return "Body for model " + carSeriesNumber + " has just been build";
    }

    public String buildingInterior() {
        return "Interior for model " + carSeriesNumber + " has just been build";
    }

    public String buildCar() {
        return buildingEngine() + "\n" + buildingChassis() + "\n" + buildingBody() + "\n" + buildingInterior()+ "\n" + "\n" ;
    }
}
