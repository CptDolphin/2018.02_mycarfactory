package pl.anicos.patterns.exercises.factorymethod.MyCarFactory;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        Car car = new Car();
        CarLogger carLogger = new CarLogger();

        try {
            carLogger.loguj(car);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
