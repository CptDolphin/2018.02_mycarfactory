package pl.anicos.patterns.exercises.factorymethod.MyCarFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class CarLogger {
    private static final String FILENAME = "carFactoryLogger.txt";
    private static CarLogger instance = null;

    public void loguj(Car car) throws IOException {
        Writer writer;

        writer = new BufferedWriter(new FileWriter(FILENAME,true));
        writer .write(car.buildCar());

        writer .flush();
        writer .close();
    }

    public static CarLogger getInstance() {
        if (instance == null) {
            instance = new CarLogger();
        }
        return instance;
    }
}
